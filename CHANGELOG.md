## [2.7.2](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.7.1...v2.7.2) (2025-02-01)


### Bug Fixes

* **dockerfile:** Update git version to `2.47.2-r0` ([ead5ea7](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/ead5ea719d80c8fc4a3dc950cb6a5f6e75f6a629))
* **generateDocs.js:** Update doc strings ([59c1118](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/59c1118798611116071d51bfd3c0da2ba5f9dc66))

## [2.7.1](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.7.0...v2.7.1) (2024-12-29)


### Bug Fixes

* **generate:** Add notes enabled to generate test ([d432039](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/d43203985113a0bcaf4b5dbdc9490aebe246a568))

# [2.7.0](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.6.0...v2.7.0) (2024-12-29)


### Features

* **cleanup:** Add remove-empty-keys command ([fb1ffe7](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/fb1ffe7fe104e1fd85659448b70782f599822072))

# [2.6.0](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.5.6...v2.6.0) (2024-12-27)


### Features

* **generate:** Add overwrite function to set custom replica count ([a43fdf3](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/a43fdf3b96a494b7fe86dbb30154e1b39bb03ed0))

## [2.5.6](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.5.5...v2.5.6) (2024-12-24)


### Bug Fixes

* **src:** Skip if valuePath undefined and null ([85af86a](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/85af86a0279ab548b9ced031e16fdf7124df728e))

## [2.5.5](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.5.4...v2.5.5) (2024-12-24)


### Bug Fixes

* **src:** Set range to support multiline ([d3bb21f](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/d3bb21f6a72350751d925c928f39d9b98ba51659))

## [2.5.4](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.5.3...v2.5.4) (2024-12-24)


### Bug Fixes

* **src:** Add replace all for generate for range ([9ca4422](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/9ca4422f3fb37242e468d0e88b5870b9d76d7e64))

## [2.5.3](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.5.2...v2.5.3) (2024-12-10)


### Bug Fixes

* **Dockerfile:** Update to alpine 3.21 ([0b8cbe3](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/0b8cbe386f852a26206842b3a94fad26a6d3c6be))
* **src:** Use .yaml.gotmpl for all openDesk file suffixes ([923b434](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/923b434ebf361e4c3ffbb1cc10da4230e70d4fc1))

## [2.5.2](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.5.1...v2.5.2) (2024-10-13)


### Bug Fixes

* **src:** Update generateDocs to support helmfile.yaml.gotmpl ([5f38ea1](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/5f38ea16f34e7c88c93dbabf6b78e326326c4956))

## [2.5.1](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.5.0...v2.5.1) (2024-10-12)


### Bug Fixes

* Bump gitlab-config to 2.4.2 ([422c5c2](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/422c5c2989b4b852e3e62bde33ba48e5af90833c))
* Text for security-context.md ([41e9537](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/41e95371d0f590d6a3b5ea5a10de56c39d28c1c1))

# [2.5.0](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.4.4...v2.5.0) (2024-09-18)


### Features

* **src:** Add generateKyvernoTests command ([79c4367](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/79c436701fd8f852375c50c60befade7b9c0e9dd))

## [2.4.4](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.4.3...v2.4.4) (2024-06-09)


### Bug Fixes

* Use child helmfile and catch errors in manifest parsing ([4a6e0fa](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/4a6e0fab759dd4d6c26f162b3250ae45b5a52b24))

## [2.4.3](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.4.2...v2.4.3) (2024-02-20)


### Bug Fixes

* **filterForKinds:** Add Ingress to filter ([2daaebb](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/2daaebbef5333be051c9685911a04a851d8879b8))

## [2.4.2](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.4.1...v2.4.2) (2024-02-13)


### Bug Fixes

* **generate:** Use recustion in generateDocs to find infinite levels of securityContext ([801509d](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/801509d735caca5b18f37e6ea2eec6a0ab1d33e6))

## [2.4.1](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.4.0...v2.4.1) (2024-02-11)


### Bug Fixes

* **generate:** Add license header to generate doc ([b149e3e](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/b149e3e928a1c6846ae607bead5ebf810f59e02c))

# [2.4.0](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.3.1...v2.4.0) (2024-02-11)


### Features

* **commands:** Add generate-docs command ([3075ea6](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/3075ea6071f5cd4c11c3ed1a3d4493b4801aa0a4))

## [2.3.1](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.3.0...v2.3.1) (2024-02-01)


### Bug Fixes

* **generateKyvernoTests:** Optionally remove helm test files ([e703e24](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/e703e245a4cfadf1ef86de34dd79afc62f674560))

# [2.3.0](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.2.0...v2.3.0) (2024-02-01)


### Features

* **generateKyvernoTests:** Generate Testcases by generated resources in Manifest ([ebe7b6b](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/ebe7b6b0f7c5a98180107d524ad3621ed7862e3e))

# [2.2.0](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.1.1...v2.2.0) (2024-01-21)


### Features

* **generateKyvernoTests:** Add option to filter for type and app ([c6cabf0](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/c6cabf0b62acaf67f108136d2949be1b028f2c29))

## [2.1.1](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.1.0...v2.1.1) (2024-01-21)


### Bug Fixes

* **src:** Use new kyverno schema ([999619d](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/999619d292d581ebb206d526c852d0f3307920d3))

# [2.1.0](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.0.2...v2.1.0) (2024-01-18)


### Features

* **generateKyvernoTests:** Add excludeKinds for policies ([60e14ca](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/60e14ca61e90e1288959dc6dfb9dda7599865495))

## [2.0.2](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.0.1...v2.0.2) (2024-01-17)


### Bug Fixes

* **filterForKinds:** Fix error with empty manifests ([1f49aa1](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/1f49aa1ecf7ac2b8696b85fc097bd28933f9dc3b))

## [2.0.1](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v2.0.0...v2.0.1) (2024-01-17)


### Bug Fixes

* **generate:** Change resource file to opendesk.yaml ([527a9a4](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/527a9a4080753c821f81fb9d00f13010034230da))

# [2.0.0](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v1.0.1...v2.0.0) (2024-01-17)


### Features

* Restructur sort commands and add generate and filter commands ([e6959af](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/e6959af460816a992513e45667b92e8ee61234c1))


### BREAKING CHANGES

* Sort commands use options instead of arguments

## [1.0.1](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/compare/v1.0.0...v1.0.1) (2024-01-08)


### Bug Fixes

* **Dockerfile:** Add git which is used in CI ([5952120](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/5952120af771a4c220993fa87d54e2e2e33032be))

# 1.0.0 (2024-01-08)


### Bug Fixes

* **ci:** Update npm version set command ([a3c8cb5](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/a3c8cb58b88bdefcb387604872ec4c6ae79ceafd))
* **README:** Update Binary usage method ([095fc25](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/095fc252e77e168282304d1e89cde3c13094ff8e))


### Features

* Initial commit ([dc240a6](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/commit/dc240a69ee536212b71d02eb9e07cf4832fd2801))
<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
