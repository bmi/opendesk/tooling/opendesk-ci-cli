# SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
# SPDX-License-Identifier: Apache-2.0
FROM docker.io/node:20-alpine3.21@sha256:426f843809ae05f324883afceebaa2b9cab9cb697097dbb1a2a7a41c5701de72

LABEL org.opencontainers.image.authors="Bundesministerium des Innern und für Heimat" \
      org.opencontainers.image.documentation=https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/-/blob/main/README.md \
      org.opencontainers.image.source=https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli \
      org.opencontainers.image.vendor="Bundesministerium des Innern und für Heimat" \
      org.opencontainers.image.licenses=Apache-2.0 \
      org.opencontainers.image.base.name=docker.io/node:20-alpine3.21 \
      org.opencontainers.image.base.digest=sha256:426f843809ae05f324883afceebaa2b9cab9cb697097dbb1a2a7a41c5701de72

RUN apk add --no-cache \
      git==2.47.2-r0

WORKDIR /app

COPY src/ src/
COPY package.json \
     package-lock.json \
     ./

RUN npm config set update-notifier false \
 && npm ci

ENTRYPOINT ["node", "src/index.js"]
