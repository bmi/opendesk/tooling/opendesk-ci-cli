<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# openDesk CI CLI

This repository contains a small NodeJS based CLI tool which is used mostly in CI Pipelines.

## TL;DR;

```shell
opendesk-ci -h
```

```text
Usage: opendesk-ci [options] [command]

A command-line utility to modify openDesk files.

Options:
  -h, --help                              display help for command

Commands:
  sort-all [options]                      Sort all supported openDesk environment file
  sort-charts [options]                   Sort openDesk charts environment file
  sort-images [options]                   Sort openDesk images environment file
  validate-apps [options] [app]           Validate openDesk app conformance
  generate-kyverno-tests [options] [app]  Generate kyverno test file.
  generate-kyverno-env [options]          Generate kyverno test env.
  generate-docs [options]                 Generate openDesk documentation
  filter-for-kinds [options]              Filter a YAML for for specific kubernetes kinds.
  help [command]                          display help for command
```


## Usage

There are several options to use this tool:

### Node

When you have NodeJS installed, you can start the cli tool with node command:

```shell
node src/index.js
```

### Container

You can use the provided container image:

```shell
docker run --rm -v /your/dir/helmfile:/app/helmfile -h registry.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli:latest
```

### Binary

You can download a single static binary which includes the CLI and NodeJS.

Releases can be found in: [Package Registry](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli/-/packages)

```shell
chmod +x opendesk-ci
sudo mv opendesk-ci /usr/local/bin/opendesk-ci
```

## License

This project uses the following license: Apache-2.0

## Copyright

Copyright © 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
