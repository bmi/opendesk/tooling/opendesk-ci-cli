// SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
// SPDX-License-Identifier: Apache-2.0

const {getFileContents, getYamlMultiObject} = require("../../common/getYamlFileContents");
const {writeFileSync} = require("fs");

let manifest;

async function removeEmptyKeys() {
    const file = this.opts().file;

    const fileContent = getFileContents(file);
    const fileContentYAML = getYamlMultiObject(fileContent);
    let fileContentYAMLReduced = [];

    for (const manifestIndex in fileContentYAML) {
        manifest = fileContentYAML[manifestIndex];
        try {
            if (manifest.hasOwnProperty("contents")) {
                if (manifest.contents.hasOwnProperty("value") && manifest.contents.value === null) {
                    console.log("Removing: " + "file_" + manifestIndex)
                    continue;
                }
                removeEmtpyKeysRecursive(manifest.contents, "file_" + manifestIndex);
                fileContentYAMLReduced.push(manifest);
            }

        } catch (e) {
            console.error("Error parsing manifest:");
            console.log(e)
            console.log(manifest.toString());
        }
    }
    let writeFileContent= "";
    for (const manifestIndex in fileContentYAMLReduced) {
        const manifest = fileContentYAMLReduced[manifestIndex];
        try {
            writeFileContent += manifest.toString({
                defaultKeyType: "PLAIN",
                defaultStringType: "QUOTE_DOUBLE",
                directives: true,
                lineWidth: 240,
                singleQuote: false,
            });
        } catch (e) {
            console.error(e);
            console.log(manifest)
        }
    }
    writeFileSync(file, writeFileContent);
}

function removeEmtpyKeysRecursive(item, path) {
    const indexToRemove = []
    for (const key in item.items) {
        const current = item.items[key]

        if (current.value !== undefined) {
            const type = current.value.constructor.name;
            let nextPath = path;
            if (current.key !== undefined && current.key.hasOwnProperty("value")) {
                nextPath = path + "." + current.key.value
            }

            if (current.value.value === null) {
                console.log("Removing: " + nextPath)
                indexToRemove.push(key);
            }
            removeEmtpyKeysRecursive(current.value, nextPath)
        }
        else {
            removeEmtpyKeysRecursive(current, path + "[" + key + "]")
        }
    }
    // Remove the items here, otherwise index is move cause items getting removed before processed.
    // The push function puts them in reverse order, so no index confusion can occur.
    for (const index of indexToRemove) {
        item.items.splice(index, 1)
    }
}

module.exports = {
    removeEmptyKeys
}
