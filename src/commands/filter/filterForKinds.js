// SPDX-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
// SPDX-License-Identifier: Apache-2.0

const {getFileContents, getYamlMultiObject} = require("../../common/getYamlFileContents");
const {writeFileSync} = require("fs");

async function filterForKinds() {
    const file = this.opts().file;
    const kinds = [
        "StatefulSet",
        "Deployment",
        "Job",
        "Pod",
        "DaemonSet",
        "PersistentVolumeClaim",
        "Ingress",
    ];

    const fileContent = getFileContents(file);
    const fileContentYAML = getYamlMultiObject(fileContent);
    let fileContentYAMLReduced = [];

    for (const manifestIndex in fileContentYAML) {
        const manifest = fileContentYAML[manifestIndex];
        try {
            const kind = manifest.contents.get("kind");
            if (kinds.includes(kind)) {
                fileContentYAMLReduced.push(manifest);
            }
        } catch (e) {
            console.error("Error parsing manifest:");
            console.log(manifest.toString());
        }
    }

    let writeFileContent= "";
    for (const manifestIndex in fileContentYAMLReduced) {
        const manifest = fileContentYAMLReduced[manifestIndex];
        try {
            writeFileContent += manifest.toString({
                defaultKeyType: "PLAIN",
                defaultStringType: "QUOTE_DOUBLE",
                directives: true,
                lineWidth: 240,
                singleQuote: false,
            });
        } catch (e) {
            console.error(e);
            console.log(manifest)
        }
    }
    writeFileSync(file, writeFileContent);
}

module.exports = {
    filterForKinds
}
