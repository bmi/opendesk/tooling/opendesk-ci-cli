// SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
// SPDX-License-Identifier: Apache-2.0
const {getFileContents, getYamlObject, getYamlMultiObject} = require("../../common/getYamlFileContents");
const json2md = require("json2md")
const {listAllApps, getHelmfileYaml, getHelmfileReleases} = require("../../common/apps");
const {writeFileSync} = require("fs");

const notAvailableSymbol = "n/a";

async function generateDocs() {
  const directory = this.opts().directory;
  const appsDirectory = directory + "/helmfile/apps";

  const appList = listAllApps(appsDirectory);

  const apps = {}

  for(const appIndex in appList) {
    const currentApp = appList[appIndex];

    const appDirectory = appsDirectory + "/" + currentApp;
    const helmfileYaml = getHelmfileYaml(appDirectory);

    // Iterate over all releases
    const releases = getHelmfileReleases(helmfileYaml);
    for (let releaseIndex in releases) {
      const release = releases[releaseIndex];
      const name = release.get("name");
      const values = release.get("values").items;

      // Iterate over all values files
      for (let valuesIndex in values) {
        const valuePath = values[valuesIndex].value;

        if (valuePath !== undefined && valuePath !== "" && valuePath !== null) {
          const path = appDirectory + "/" + valuePath;
          const fileContent = getFileContents(path).replaceAll(/{{-?\s? if.*?end\s?}}/gm, "replaced")
          const yamls = getYamlMultiObject(fileContent);
          for (let yaml of yamls) {
            addSecurityContextsToApps(apps, yaml.contents.items, "**" + currentApp + "**", name)
          }
        }
      }
    }
  }
  writeSecurityContextMD(directory, apps);
}

function addSecurityContextsToApps(apps, items, name, parentKey) {
  let key=name;
  if (parentKey !== undefined) {
    key =  name + "/" + parentKey;
  }
  for (const index in items) {
    const content = items[index]
    if (content.hasOwnProperty("key")
      && (content.key.value === "containerSecurityContext" || content.key.value === "securityContext")) {
      apps[key] = content.value;
    }
    if (content.hasOwnProperty("key") && content.key.constructor.name === "Scalar") {
      addSecurityContextsToApps(apps, content.value.items, key, content.key.value)
    }
  }
}

function generateRows(apps) {
  const rows = []

  for (const [key, value] of Object.entries(apps)) {
    rows.push(
      generateRow(
        key,
        value.get("allowPrivilegeEscalation"),
        value.get("privileged"),
        value.get("readOnlyRootFilesystem"),
        value.get("runAsNonRoot"),
        value.get("runAsUser"),
        value.get("runAsGroup"),
        value.get("seccompProfile"),
        value.get("capabilities")
      )
    )
  }
  rows.sort(function(a, b) {
    const keyA = a.process, keyB = b.process;
    if (keyA < keyB) return -1;
    if (keyA > keyB) return 1;
    return 0;
  });
  return rows;
}

function generateTable(rows) {
  return {
    table: {
      headers: [
        "process",
        "status",
        "allowPrivilegeEscalation",
        "privileged",
        "readOnlyRootFilesystem",
        "runAsNonRoot",
        "runAsUser",
        "runAsGroup",
        "seccompProfile",
        "capabilities"
      ],
      rows: rows
    }
  }
}

function generateRow(
  process,
  allowPrivilegeEscalation,
  privileged,
  readOnlyRootFilesystem,
  runAsNonRoot,
  runAsUser,
  runAsGroup,
  seccompProfile,
  capabilities
) {
  const processedAllowPrivilegeEscalation = parseBool(allowPrivilegeEscalation);
  const processedPrivileged = parseBool(privileged);
  const processedReadOnlyRootFilesystem = parseBool(readOnlyRootFilesystem);
  const processedRunAsNonRoot = parseBool(runAsNonRoot);
  const processedRunAsUser = parseRaw(runAsUser);
  const processedRunAsGroup = parseRaw(runAsGroup);
  const processedSeccompProfile = parseSeccompProfile(seccompProfile);
  const processedCapabilities = parseCapabilities(capabilities);

  let result = ":white_check_mark:";
  if (processedAllowPrivilegeEscalation !== "no" ||
      processedPrivileged !== "no" ||
      processedReadOnlyRootFilesystem !== "yes" ||
      processedRunAsNonRoot !== "yes" ||
      processedRunAsUser === notAvailableSymbol ||
      processedRunAsUser === "0" ||
      processedRunAsGroup === notAvailableSymbol ||
      processedRunAsGroup === "0" ||
      processedSeccompProfile === "no" ||
      processedCapabilities !== "yes"
  ) {
    result = ":x:";
  }

  return {
    process: process,
    status: result,
    allowPrivilegeEscalation: processedAllowPrivilegeEscalation,
    privileged: processedPrivileged,
    readOnlyRootFilesystem: processedReadOnlyRootFilesystem,
    runAsNonRoot: processedRunAsNonRoot,
    runAsUser: processedRunAsUser,
    runAsGroup: processedRunAsGroup,
    seccompProfile: processedSeccompProfile,
    capabilities: processedCapabilities
  }
}

function parseBool(value) {
  if (value === "" || value === undefined) {
    return notAvailableSymbol;
  } else if (value === true || value === "true") {
    return "yes"
  }
  return "no"
}

function parseRaw(value) {
  if (value === undefined || value === "") {
    return notAvailableSymbol
  }
  return value;
}

function parseSeccompProfile(value) {
  if (value === undefined || value === "") {
    return notAvailableSymbol
  } else if (value.has("type")) {
    if (value.get("type") === "RuntimeDefault") {
      return "yes"
    }
  }
  return "no"
}

function parseCapabilities(value) {
  if (value === undefined || value === "") {
    return "no"
  } else if (value.has("add")) {
    return "no " + value.get("add").toString()
  }
  if (value.has("drop") && value.get("drop").has(0) && value.get("drop").get(0).toUpperCase() === "ALL") {
      return "yes"
  }
  return "no"
}

function writeSecurityContextMD(directory, apps) {
  const header = "<!--\n"
  + "SPDX-FileCopyrightText: 2025 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH\n"
  + "SPDX-License-Identifier: Apache-2.0\n"
  + "-->\n"
  + "<h1>Kubernetes Security Context</h1>\n\n"
  + "<!-- TOC -->\n"
  + "* [Container Security Context](#container-security-context)\n"
  + "  * [allowPrivilegeEscalation](#allowprivilegeescalation)\n"
  + "  * [capabilities](#capabilities)\n"
  + "  * [privileged](#privileged)\n"
  + "  * [runAsUser](#runasuser)\n"
  + "  * [runAsGroup](#runasgroup)\n"
  + "  * [seccompProfile](#seccompprofile)\n"
  + "  * [readOnlyRootFilesystem](#readonlyrootfilesystem)\n"
  + "  * [runAsNonRoot](#runasnonroot)\n"
  + "* [Status quo](#status-quo)\n"
  + "<!-- TOC -->\n"
  + "\n";
  const doc = []
  doc.push({
    h1: "Container Security Context"
  });
  doc.push({
    p: "The containerSecurityContext is the most important security-related section because it has the highest " +
       "precedence and restricts the container to its minimal privileges."
  });
  doc.push({
    h2: "allowPrivilegeEscalation"
  });
  doc.push({
    p: "Privilege escalation (such as via set-user-ID or set-group-ID file mode) should not be allowed (Linux only) " +
       "at any time."
  });
  doc.push({
    code: {
      language: "yaml",
      content: [
        "containerSecurityContext:",
        "  allowPrivilegeEscalation: false"
      ]
    }
  });
  doc.push({
    h2: "capabilities"
  });
  doc.push({
    p: "Containers must drop ALL capabilities, and are only permitted to add back the `NET_BIND_SERVICE` capability " +
       "(Linux only)."
  });
  doc.push({
    p: "**Optimal:**"
  });
  doc.push({
    code: {
      language: "yaml",
      content: [
        "containerSecurityContext:",
        "  capabilities:",
        "    drop:",
        "      - \"ALL\""
      ]
    }
  });
  doc.push({
    p: "**Allowed:**"
  });
  doc.push({
    code: {
      language: "yaml",
      content: [
        "containerSecurityContext:",
        "  capabilities:",
        "    drop:",
        "      - \"ALL\"",
        "    add:",
        "      - \"NET_BIND_SERVICE\""
      ]
    }
  });
  doc.push({
    h2: "privileged"
  });
  doc.push({
    p: "Privileged Pods eliminate most security mechanisms and must be disallowed."
  });
  doc.push({
    code: {
      language: "yaml",
      content: [
        "containerSecurityContext:",
        "  privileged: false"
      ]
    }
  });
  doc.push({
    h2: "runAsUser"
  });
  doc.push({
    p: "Containers should set a user id >= 1000 and never use 0 (root) as user."
  });
  doc.push({
    code: {
      language: "yaml",
      content: [
        "containerSecurityContext:",
        "  runAsUser: 1000"
      ]
    }
  });
  doc.push({
    h2: "runAsGroup"
  });
  doc.push({
    p: "Containers should set a group id >= 1000 and never use 0 (root) as user."
  });
  doc.push({
    code: {
      language: "yaml",
      content: [
        "containerSecurityContext:",
        "  runAsGroup: 1000"
      ]
    }
  });
  doc.push({
    h2: "seccompProfile"
  });
  doc.push({
    p: "The seccompProfile must be explicitly set to one of the allowed values. An unconfined profile and the complete " +
       "absence of the profile are prohibited."
  });
  doc.push({
    code: {
      language: "yaml",
      content: [
        "containerSecurityContext:",
        "  seccompProfile:",
        "    type: \"RuntimeDefault\""
      ]
    }
  });
  doc.push({
    p: "or"
  });
  doc.push({
    code: {
      language: "yaml",
      content: [
        "containerSecurityContext:",
        "  seccompProfile:",
        "    type: \"Localhost\""
      ]
    }
  });
  doc.push({
    h2: "readOnlyRootFilesystem"
  });
  doc.push({
    p: "Containers should have an immutable file systems, so that attackers can not modify application code " +
       "or download malicious code."
  });
  doc.push({
    code: {
      language: "yaml",
      content: [
        "containerSecurityContext:",
        "  readOnlyRootFilesystem: true"
      ]
    }
  });
  doc.push({
    h2: "runAsNonRoot"
  });
  doc.push({
    p: "Containers must be required to run as non-root users."
  });
  doc.push({
    code: {
      language: "yaml",
      content: [
        "containerSecurityContext:",
        "  runAsNonRoot: true"
      ]
    }
  });
  doc.push({
    h1: "Status quo"
  });
  doc.push(
    [
      {
        p: "openDesk aims to ensure that all security relevant settings are explicitly templated and comply with " +
           "security recommendations."
      }, {
        p: "The rendered manifests are also validated against Kyverno [policies](/.kyverno/policies) in CI to ensure " +
           "that the provided values inside openDesk are properly templated by the Helm charts."
      }, {
        p: "This list gives you an overview of templated security settings and if they comply with security standards:"
      }
    ]
  );
  doc.push({
    ul: [
      "**yes**: Value is set to `true`",
      "**no**: Value is set to `false`",
      `**${notAvailableSymbol}**: Not explicitly templated in openDesk; default is used.`
    ]
  });
  doc.push(generateTable(generateRows(apps)))
  doc.push({
    p: "This file is auto-generated by " +
       "[openDesk CI CLI](https://gitlab.opencode.de/bmi/opendesk/tooling/opendesk-ci-cli)"
  });

  writeFileSync(directory + "/docs/security-context.md", header + json2md(doc));
}

module.exports = {
    generateDocs
}
