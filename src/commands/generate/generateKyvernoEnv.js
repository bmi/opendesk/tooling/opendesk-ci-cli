// SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
// SPDX-License-Identifier: Apache-2.0
const {Document} = require("yaml");
const {getFileContents, getJSObject} = require("../../common/getYamlFileContents");
const {writeFileSync} = require("fs");

async function generateKyvernoEnv(){
    const directory = this.opts().directory;
    const overwrites = this.opts().overwrites;

    // Write test env
    createTestEnv(directory, overwrites);
}

function createTestEnv(directory, overwrites) {
    const defaultEnvironment = directory + "/default";
    const ciEnvironmentFile = directory + "/test/values.yaml.gotmpl";

    const ciJSON = {
        global: {
            imageRegistry: "my_private_registry.domain.tld",
            imagePullSecrets: [
                "kyverno-test"
            ],
            imagePullPolicy: "kyverno",
        },
        persistence: {
            storageClassNames: {
                RWX: "kyverno-test",
                RWO: "kyverno-test",
            }
        },
        ingress: {
            ingressClassName: "kyverno",
            tls: {
                enabled: true,
                secretName: "kyverno-tls",
            }
        },
        notes: {
            enabled: true,
        }
    }
    const outputContent = new Document(ciJSON);

    // Overwrites
    let overwriteMap = {}
    if (overwrites !== undefined) {
        const overwriteContent = getFileContents(overwrites);
        const overwriteJSON = getJSObject(overwriteContent);
        overwriteMap = overwriteJSON.replicas;
    }

    // Replicas
    const replicasContent = getFileContents(defaultEnvironment + "/replicas.yaml.gotmpl");
    const replicas = getJSObject(replicasContent);

    let scaledReplicas = {}
    for (const replicaIndex in replicas.replicas) {
        if (replicaIndex in overwriteMap) {
            scaledReplicas[replicaIndex] = overwriteMap[replicaIndex];
        } else {
            scaledReplicas[replicaIndex] = 42
        }
    }
    outputContent.set("replicas", scaledReplicas);

    // Persistence
    const persistenceContent = getFileContents(defaultEnvironment + "/persistence.yaml.gotmpl");
    const persistence = getJSObject(persistenceContent);

    const scaledPersistence = replaceValuesInPersistence({}, persistence.persistence.size, "")
    outputContent.get("persistence").set("size", scaledPersistence);

    // Write the YAML file.
    writeFileSync(ciEnvironmentFile, outputContent.toString({
        defaultKeyType: "PLAIN",
        defaultStringType: "QUOTE_DOUBLE",
        directives: true,
        lineWidth: 120,
        singleQuote: false
    }));
}

function replaceValuesInPersistence(scaledPersistence, size, prevIndex) {
    for (let index in size) {
        const persistenceSize = size[index];
        if (typeof persistenceSize === 'object') {
            scaledPersistence[index] = {}
            replaceValuesInPersistence(scaledPersistence, persistenceSize, index)
        } else {
            if (prevIndex) {
                scaledPersistence[prevIndex][index] = persistenceSize.replace(/\d+/i, 42)
            } else {
                scaledPersistence[index] = persistenceSize.replace(/\d+/i, 42)
            }
        }
    }
    return scaledPersistence;
}

module.exports = {
    generateKyvernoEnv
}
