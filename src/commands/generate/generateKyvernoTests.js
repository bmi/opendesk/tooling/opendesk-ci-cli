// SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
// SPDX-License-Identifier: Apache-2.0
const {Document, YAMLMap, YAMLSeq} = require("yaml");
const {getFileContents, getJSObject, getYamlMultiObject} = require("../../common/getYamlFileContents");
const {writeFileSync, readdirSync} = require("fs");

async function generateKyvernoTests(app){
    const directory = this.opts().directory;
    const type = this.opts().type;
    const source = this.opts().source;
    const file = this.opts().file;
    const skipTests = this.opts().skipTests;

    const kyvernoTestYAML = new Document();
    // Split SPDX heder in JavaScript to prevent detection of reuse linter.
    kyvernoTestYAML.commentBefore = "# SPDX"
      + "-FileCopyrightText: 2024 Bundesministerium des Innern und für Heimat, PG ZenDiS \"Projektgruppe für Aufbau "
      + "ZenDiS\"\n# SPDX"
      + "-License-Identifier: Apache-2.0";
    kyvernoTestYAML.directives.docStart = true;
    kyvernoTestYAML.directives.docEnd = true;

    // Header
    kyvernoTestYAML.set("apiVersion", "cli.kyverno.io/v1alpha1");
    kyvernoTestYAML.set("kind", "Test");
    // Metadata
    const metadataMap = new YAMLMap();
    metadataMap.set ("name", "opendesk-test");
    kyvernoTestYAML.set("metadata", metadataMap);

    // Policies
    const policiesPath = "policies";
    const policies = listAvailablePolicies(directory + "/" + policiesPath);
    const policiesSeq = new YAMLSeq();
    for (const policyIndex in policies) {
        const policy = policies[policyIndex];
        policiesSeq.add(policiesPath + "/" + policy);
    }
    kyvernoTestYAML.set("policies", policiesSeq);

    // Resources
    const resourcesSeq= new YAMLSeq();
    resourcesSeq.add("opendesk.yaml");
    kyvernoTestYAML.set("resources", resourcesSeq);

    // Tests
    let appsToTest
    if (source === "manifest") {
        appsToTest = seedAppsByManifest(directory, app, file, skipTests);
    } else {
        appsToTest = seedAppsByFile(directory, app, file);
    }
    const policiesToTest = seedPolicies(directory + "/" + policiesPath, type);
    const tests = new YAMLSeq();
    for (let currentAppIndex in appsToTest) {
        const currentApp = appsToTest[currentAppIndex];
        for (let currentPolicyIndex in policiesToTest) {
            const currentPolicies = policiesToTest[currentPolicyIndex];
            if (currentPolicies.excludeKinds.includes(currentApp.kind)) {
                console.log(`Skipping ${currentApp.kind}/${currentApp.resource} for ${currentPolicies.rule}`);
                continue;
            }
            if (!currentPolicies.kinds.includes(currentApp.kind)) {
                continue;
            }
            const currentTest = new YAMLMap();
            const resourceMap = new YAMLSeq();
            resourceMap.add(currentApp.resource)
            currentTest.set("resources", resourceMap);
            currentTest.set("policy", currentPolicies.name);
            currentTest.set("kind", currentApp.kind);
            currentTest.set("rule", currentPolicies.rule);
            currentTest.set("result", "pass");
            tests.add(currentTest);
        }
    }
    kyvernoTestYAML.set("results", tests);

    // Write final file
    const filePath = directory + "/kyverno-test.yaml";
    writeFileSync(filePath, kyvernoTestYAML.toString({
        defaultKeyType: "PLAIN",
        defaultStringType: "QUOTE_DOUBLE",
        directives: true,
        lineWidth: 120,
        singleQuote: false
    }));
}

function App(resource, kind) {
    this.resource = resource;
    this.kind = kind;
}

function Policy(name, rule, excludeKinds, kinds) {
    this.name = name;
    this.rule = rule;
    this.excludeKinds = excludeKinds;
    this.kinds = kinds;
}

function seedAppsByFile(directory, app, file) {
    const apps = [];
    const appsControl = getFileContents(directory + "/" + file);
    const appsObj = getJSObject(appsControl);
    for (const currentAppIndex in appsObj["pod"]) {
        const currentApp = appsObj["pod"][currentAppIndex];
        if (app === "all" || app === currentApp.app) {
            apps.push(new App(currentApp.resource, currentApp.kind));
        }
    }
    return apps;
}

function seedAppsByManifest(directory, app, file, skipTests) {
    const apps = [];
    const manifest = getFileContents(directory + "/" + file);
    const manifestYAML = getYamlMultiObject(manifest);

    for (const manifestIndex in manifestYAML) {
        const manifest = manifestYAML[manifestIndex];
        try {
            const kind = manifest.contents.get("kind");
            const name = manifest.contents.get("metadata").get("name");
            if (kind === "Pod" && skipTests === "true") {
                const annotations = manifest.contents.get("metadata").get("annotations");
                if (annotations.has("helm.sh/hook")) {
                    if (annotations.get("helm.sh/hook") === "test") {
                        continue;
                    }
                }
            }
            apps.push(new App(name, kind));
        } catch (e) {
            console.error("Error parsing manifest:" + e);
        }
    }
    return apps;
}

function seedPolicies(policiesDirectory, type) {
    const policies = [];
    const policiesControl = getFileContents(policiesDirectory + "/_policies.yaml");
    const policiesObj = getJSObject(policiesControl);
    for (const currentPolicyIndex in policiesObj["pod"]) {
        const currentPolicy = policiesObj["pod"][currentPolicyIndex];
        let excludeKinds = []
        if (currentPolicy.hasOwnProperty("excludeKinds")){
            excludeKinds = currentPolicy.excludeKinds
        }
        let kinds = []
        if (currentPolicy.hasOwnProperty("kinds")){
            kinds = currentPolicy.kinds
        }
        if (type === undefined || currentPolicy.type === type) {
            policies.push(new Policy(currentPolicy.name, currentPolicy.rule, excludeKinds, kinds));
        }
    }
    return policies;
}

function listAvailablePolicies(policiesDirectory) {
    let directoryContent;
    try {
        directoryContent = readdirSync(policiesDirectory);
    } catch (e) {
        console.error(e.message);
        return null;
    }
    directoryContent.splice(directoryContent.indexOf("_policies.yaml"), 1);
    return directoryContent;
}

module.exports = {
    generateKyvernoTests
}
