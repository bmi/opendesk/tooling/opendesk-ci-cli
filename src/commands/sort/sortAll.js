// SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
// SPDX-License-Identifier: Apache-2.0

const {sortCharts} = require("./sortCharts");
const {sortImages} = require("./sortImages");

async function sortAll() {
    await Promise.all([
        sortImages(this.opts().directory + "/environments/default/images.yaml.gotmpl"),
        sortCharts(this.opts().directory + "/environments/default/charts.yaml.gotmpl"),
    ]);
}

module.exports = {
    sortAll
}
