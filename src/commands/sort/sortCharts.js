// SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
// SPDX-License-Identifier: Apache-2.0

const { sort } = require("../../common/sort.js");

async function sortCharts(path) {
    let chartsPath;
    if (path !== undefined) {
        chartsPath = path;
    } else {
        chartsPath = this.opts().file;
    }

    const destinationSort = [
        "registry",
        "repository",
        "name",
        "version",
        "verify"
    ]

    await sort("charts", destinationSort, chartsPath)
}

module.exports = {
    sortCharts
}
