// SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
// SPDX-License-Identifier: Apache-2.0

const { sort } = require("../../common/sort.js");

async function sortImages(path) {
    let imagesPath;
    if (path !== undefined) {
        imagesPath = path;
    } else {
        imagesPath = this.opts().file;
    }

    const destinationSort = [
        "registry",
        "repository",
        "tag"
    ];

    await sort("images", destinationSort, imagesPath);
}

module.exports = {
    sortImages
}
