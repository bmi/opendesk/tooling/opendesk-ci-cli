// SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
// SPDX-License-Identifier: Apache-2.0

const {checkAppExists, getHelmfileReleases, getHelmfileYaml, listAllApps} = require("../../common/apps");
const {getFileContents} = require("../../common/getYamlFileContents");
const {
    hasImageSection,
    validateReplicas,
    validateResources,
    validateImagePullSecrets,
    validateSecurityContext
} = require("../../common/validateField");

async function validateFields(app) {
    const appsDirectory = this.opts().directory + "/apps";

    let appList = []
    if (app === "all") {
        appList = listAllApps(appsDirectory);
    } else {
        if (!checkAppExists(app, appsDirectory)) {
            console.error(`App ${app} does not exists in directory`);
        }
        appList.push([app]);
    }
    // Failure variable
    let nonCompliantFound = false;
    let nonCompliantApps = {};

    for(const appIndex in appList) {
        const currentApp = appList[appIndex];

        const appDirectory = appsDirectory + "/" + currentApp;
        const helmfileYaml = getHelmfileYaml(appDirectory);

        nonCompliantApps[currentApp] = new Result(true);
        // Iterate over all releases
        const releases = getHelmfileReleases(helmfileYaml);
        for (let releaseIndex in releases) {
            const release = releases[releaseIndex];

            const name = release.get("name");
            const values = release.get("values").items;

            // skip bootstrap releases
            if (this.opts().skipBootstrap && name.search("bootstrap") > 0) {
                continue;
            }

            // Iterate over all values files
            let texts = []
            for (let valuesIndex in values) {
                const valuePath = values[valuesIndex].value;

                const path = appDirectory + "/" + valuePath;
                texts.push(getFileContents(path));
            }
            const results = {};

            if (hasImageSection(texts)){
                results.replicas = new Result(validateReplicas(texts));
                results.resources = new Result(validateResources(texts));
                results.imagePullSecrets  = new Result(validateImagePullSecrets(texts));
                results.securityContext = new Result(validateSecurityContext(texts));
            }

            // Check compliance
            for (const resultIndex in results) {
                const result = results[resultIndex];
                if (!result.compliant) {
                    nonCompliantFound = true;
                    nonCompliantApps[currentApp] = new Result(false);
                }
            }
            // Show compliance status
            if (this.opts().loglevel === "debug"){
                console.log(`${currentApp} |> ${name}`);
                console.table(results);
            }
        }
    }
    // Exit when noncompliance found
    if (this.opts().loglevel === "warn" || this.opts().loglevel === "debug") {
        console.table(nonCompliantApps);
    }
    if (nonCompliantFound) {
        process.exit(1);
    }
}

function Result(compliant) {
    this.compliant = compliant;
}

module.exports = {
    validateFields
}
