// SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
// SPDX-License-Identifier: Apache-2.0

const {readdirSync} = require ("fs");
const {getFileContents, getYamlMultiObject} = require("./getYamlFileContents");
function listAllApps(appsDirectory) {
    try {
        return readdirSync(appsDirectory);
    } catch (e) {
        console.error(e.message);
        return null;
    }
}

function checkAppExists(searchForApp, appsDirectory) {
    const listOfApps = listAllApps(appsDirectory);
    if (searchForApp === "all") {
        return true;
    }

    return listOfApps.includes(searchForApp);
}

function getHelmfileYaml(appDirectory) {
    const helmfilePath = appDirectory + "/helmfile-child.yaml.gotmpl";
    const helmfileContent = getFileContents(helmfilePath);
    return getYamlMultiObject(helmfileContent);
}

function getHelmfileReleases(helmfileYaml) {
    return helmfileYaml.slice(-1)[0].get("releases").items;
}

module.exports = {
    checkAppExists,
    getHelmfileReleases,
    getHelmfileYaml,
    listAllApps,
}
