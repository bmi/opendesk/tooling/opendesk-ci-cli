// SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
// SPDX-License-Identifier: Apache-2.0

const {readFileSync} = require ("fs");
const {parse, parseDocument, parseAllDocuments} = require("yaml");

function getFileContents(filePath) {
    // Load file from disk.
    let fileContent;
    try {
        fileContent = readFileSync(filePath, { encoding: "utf8"});
    } catch (e) {
        console.error(e.message);
        return null;
    }

    return fileContent;
}

function getYamlObject(fileContent) {
    let obj;

    // Parse file contents.
    try {
        obj = parseDocument(fileContent);
    } catch (e) {
        console.error(e.message);
        return null;
    }
    return obj;
}

function getYamlMultiObject(fileContent) {
    let obj;

    // Parse file contents.
    try {
        obj = parseAllDocuments(fileContent);
    } catch (e) {
        console.error(e.message);
        return null;
    }
    return obj;
}

function getJSObject(fileContent) {
    let obj;

    // Parse file contents.
    try {
        obj = parse(fileContent);
    } catch (e) {
        console.error(e.message);
        return null;
    }
    return obj;
}

module.exports = {
    getFileContents,
    getJSObject,
    getYamlObject,
    getYamlMultiObject,
}
