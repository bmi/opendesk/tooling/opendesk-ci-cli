// SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
// SPDX-License-Identifier: Apache-2.0

const {writeFileSync} = require ("fs");
const {Document, parse, parseDocument, YAMLMap} = require("yaml");
const {getFileContents, getJSObject, getYamlObject} = require("./getYamlFileContents");

async function sort(topKey,destinationSort,fileLocation) {

    // Load file from disk.
    const fileContent = getFileContents(fileLocation);
    const fileObjects = getJSObject(fileContent);
    const fileYAML = getYamlObject(fileContent);

    // Check if content exists
    if (fileContent === null || fileObjects === null || fileYAML === null) {
        return;
    }

    // Check if the file contains key.
    if (!fileObjects.hasOwnProperty(topKey)) {
        console.error(`File hast no '${topKey}' dictionary key`);
    }

    // Create YAML map with all images.
    const newItemsMap = new YAMLMap();

    // Sort items
    const objects = fileObjects[topKey];
    const sorted = Object.keys(objects).sort((a, b) => a.localeCompare(b, undefined, {sensitivity: 'base'}));

    // Iterate over all items.
    for (let index in sorted) {
        const itemKey = sorted[index];
        const seq = fileYAML.get(topKey).get(itemKey);
        const map = new YAMLMap();

        // Preserve comments
        map.commentBefore = seq.commentBefore;
        map.comment =  seq.comment;

        // Add new keys
        for (let sortKey in destinationSort) {
            const key = destinationSort[sortKey];
            if (seq.has(key)) {
                map.add({
                    key: key,
                    value: seq.get(key)
                });
            }
        }

        // Add these items to item's map.
        newItemsMap.set(itemKey, map);
    }

    // Create new YAML document.
    const newFileContent = new Document();
    newFileContent.commentBefore = fileYAML.commentBefore;
    newFileContent.directives.docStart = true;
    newFileContent.directives.docEnd = true;
    newFileContent.set(topKey, newItemsMap);

    // Update file content.
    writeFileSync(fileLocation, newFileContent.toString({
        defaultKeyType: "PLAIN",
        defaultStringType: "QUOTE_DOUBLE",
        directives: true,
        lineWidth: 120,
        singleQuote: false
    }));
}

module.exports = {
    sort
}
