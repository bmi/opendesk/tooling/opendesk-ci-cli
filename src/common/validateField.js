// SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
// SPDX-License-Identifier: Apache-2.0

function searchStringInTexts(texts, searchString) {
    let found = false;
    for (const textIndex in texts) {
        const text = texts[textIndex]
        if (text.search(searchString)>0) {
            found = true;
        }
    }
    return found;
}

function validateReplicas(texts) {
    const option1 = searchStringInTexts(texts, "{{ .Values.replicas");
    const option2 = searchStringInTexts(texts, "replicas: 1");
    const option3 = searchStringInTexts(texts, "replicaCount: 1");
    return option1 || option2 || option3;
}

function validateResources(texts) {
    return searchStringInTexts(texts, "{{ .Values.resources.");
}
function validateImagePullSecrets(texts) {
    return searchStringInTexts(texts, ".Values.global.imagePullSecrets");
}
function validateSecurityContext(texts) {
    return searchStringInTexts(texts, /securityContext/i);
}

function hasImageSection(texts) {
    return searchStringInTexts(texts, ".Values.images");
}

module.exports = {
    hasImageSection,
    validateImagePullSecrets,
    validateReplicas,
    validateResources,
    validateSecurityContext,
}
