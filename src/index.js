#! /usr/bin/env node
// SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
// SPDX-License-Identifier: Apache-2.0

const { program } = require("commander");
const { filterForKinds } = require("./commands/filter/filterForKinds");
const { generateKyvernoTests } = require("./commands/generate/generateKyvernoTests.js");
const { generateKyvernoEnv } = require("./commands/generate/generateKyvernoEnv.js");
const { sortAll } = require("./commands/sort/sortAll.js");
const { sortCharts } = require("./commands/sort/sortCharts.js");
const { sortImages } = require("./commands/sort/sortImages.js");
const { validateFields } = require("./commands/validate/validateFields.js");
const { generateDocs } = require("./commands/generate/generateDocs");
const { removeEmptyKeys } = require("./commands/cleanup/removeEmptyKeys");

program
    .name('opendesk-ci')
    .description('A command-line utility to modify openDesk files.');

// Sorting
program
    .command('sort-all')
    .description('Sort all supported openDesk environment file')
    .option('-d, --directory <path>', "openDesk ./helmfile directory path", "./helmfile")
    .action(sortAll);

program
    .command('sort-charts')
    .description('Sort openDesk charts environment file')
    .option('-f, --file <path>', "openDesk ./charts.yaml filepath", "./helmfile/environments/default/charts.yaml")
    .action(sortCharts);

program
    .command('sort-images')
    .description('Sort openDesk images environment file')
    .option('-f, --file <path>', "openDesk ./images.yaml filepath", "./helmfile/environments/default/images.yaml")
    .action(sortImages);

// Validating
program
    .command('validate-apps')
    .description('Validate openDesk app conformance')
    .option('-d, --directory <path>', "openDesk ./helmfile directory path", "./helmfile")
    .option('-l, --loglevel <loglevel>', "Show output of all checks: 'silent', 'info', 'error'", "warn")
    .option('--skip-bootstrap <skip-bootstrap>', "Skip validation of bootstrap releases", false)
    .argument("[app]", "App in ./apps directory to validate", "all")
    .action(validateFields);

// Generation
program
    .command('generate-kyverno-tests')
    .description('Generate kyverno test file.')
    .option('-d, --directory <path>', "openDesk ./.kyverno directory path", "./.kyverno")
    .option('-t, --type <type>', "Only use specific type of policies: 'required', 'optional'")
    .option('-s, --source <source>', "Generate test from 'apps' file or k8s yaml 'manifest'", "apps")
    .option('-f, --file <file>', "Filename with sources for test generation", "_apps.yaml")
    .option('--skip-tests <skip>', "Skip helm test manifests", "false")
    .argument("[app]", "App in ./apps directory to generate tests for", "all")
    .action(generateKyvernoTests);

program
    .command('generate-kyverno-env')
    .description('Generate kyverno test env.')
    .option('-d, --directory <path>', "openDesk ./helmfile/environments directory path", "./helmfile/environments")
    .option('-x, --overwrites <path>', "File path to file containing generation overwrites")
    .action(generateKyvernoEnv);

program
    .command('generate-docs')
    .description('Generate openDesk documentation')
    .option('-d, --directory <path>', "openDesk directory path")
    .action(generateDocs);

// Filter
program
    .command('filter-for-kinds')
    .description('Filter a YAML for for specific kubernetes kinds.')
    .option('-f, --file <path>', "openDesk ./.kyverno/manifets.yaml filepath", "./.kyverno/manifets")
    .action(filterForKinds);

// Cleanup
program
    .command('remove-empty-keys')
    .description('Removes empty keys from manifest.')
    .option('-f, --file <path>', "Path to yaml file")
    .action(removeEmptyKeys);

program.parse();
